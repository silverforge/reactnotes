import React, { Component } from 'react';
import {
    StyleSheet,
    TextInput,
    View
} from 'react-native';
import SimpleButton from './SimpleButton';

export default class NoteScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {note: props.note};
    }

    render() {
        console.log(this.props);

        return(
            <View style={newStyle.container}>
                <SimpleButton
                    onPress={() => this.props.navigator.push({name: 'camera', note: this.state.note})} 
                    customText="Take a picture"
                    style={newStyle.simpleButton}
                    textStyle={newStyle.simpleButtonText} />

                <View style={newStyle.inputContainer}>
                    <TextInput
                        ref="title"
                        autoFocus={true} 
                        autoCapitalize="sentences"
                        placeholder='Untitled'
                        underlineColorAndroid="transparent"
                        onEndEditing={(text) => { this.refs.bodie.focus }} 
                        style={[newStyle.textInput, newStyle.textTitle]} 
                        value={this.state.note.title}
                        onChangeText={(title) => this.updateNote(title, this.state.note.body)}
                        />
                </View>

                <View style={[newStyle.inputContainer, newStyle.body]}>
                    <TextInput
                        ref="bodie"
                        multiline={true} 
                        placeholder='Start typing'
                        textAlignVertical="top"
                        underlineColorAndroid="transparent" 
                        style={[newStyle.textInput, newStyle.body]} 
                        value={this.state.note.body}
                        onChangeText={(body) => this.updateNote(this.state.note.title, body)}
                        />
                </View>
            </View>
        );
    }

    updateNote(title, body) {
        var note = Object.assign(this.state.note, {title: title, body: body});
        this.props.onChangeNote(note);
        this.setState(note);
    }
}

var newStyle = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 64,
//        borderColor: 'red',
//        borderWidth: 5
    },
    inputContainer: {
        borderBottomColor: '#9E7CE3',
        borderBottomWidth: 1,
//        borderColor: 'blue',
//        borderWidth: 2,
        marginBottom: 16,
        marginRight: 16,
        marginLeft: 16
    },
    body: {
        flex: 1
    },
    textInput: {
        fontSize: 16
    },
    textTitle: {
        fontWeight: 'bold',
        height: 60 // because of ios
    },
    simpleButtonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        width: 120
    },
    simpleButton: {
        backgroundColor: '#5B29C1',
        borderColor: '#48209A',
        borderWidth: 1,
        borderRadius: 4,
        paddingHorizontal: 20,
        paddingVertical: 15,
        shadowColor: 'darkgrey',
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowOpacity: 0.8,
        shadowRadius: 1
    }
});