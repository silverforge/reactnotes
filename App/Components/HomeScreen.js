import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import SimpleButton from './SimpleButton';
import NoteList from './NoteList';

export default class HomeScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NoteList 
                    navigator={this.props.navigator} 
                    notes={this.props.notes}
                    onSelectNote={this.props.onSelectNote}
                    />

                <Text style={styles.noNotesText}>You haven't created any notes yet!</Text>

                <SimpleButton
                    onPress={() => this.props.navigator.push({name: 'createNote', note: {title: "", body: "", id: 0}})} 
                    customText='Create Note'
                    style={styles.simpleButton}
                    textStyle={styles.simpleButtonText}
                    />
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
//        justifyContent: 'center',
//        alignItems: 'center',
        marginTop: 64,
        marginBottom: 8,
        marginRight: 8,
        marginLeft: 8
    },
    noNotesText: {
        color: '#48209A',
        marginBottom: 10
    },
    simpleButtonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        width: 120
    },
    simpleButton: {
        backgroundColor: '#5B29C1',
        borderColor: '#48209A',
        borderWidth: 1,
        borderRadius: 4,
        paddingHorizontal: 20,
        paddingVertical: 15,
        shadowColor: 'darkgrey',
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowOpacity: 0.8,
        shadowRadius: 1
    }
});
