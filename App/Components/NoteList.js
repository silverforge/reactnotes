import React, { Component } from 'react'
import {
    Text,
    View,
    ListView,
    TouchableHighlight,
    StyleSheet
} from 'react-native'

export default class NoteList extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    }

    render() {
        return (
            <ListView 
                enableEmptySections={true}
                style={styles.listContainer}
                dataSource={this.ds.cloneWithRows(this.props.notes)}
                renderRow={(rowData) => {
                    return(
                        <TouchableHighlight onPress={() => this.props.onSelectNote(rowData)} underlayColor="#9E7CE3">
                            <View style={styles.container}>
                                <Text style={styles.textRow}>{rowData.title}</Text>
                            </View>
                        </TouchableHighlight>
                    );
                }}
                renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
            />
        );
    }

}

var styles = StyleSheet.create({
    listContainer: {
        flex: 1,
    },
    container: {
        flex: 1,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textRow: {
        flex: 0,
        fontSize: 16,
        fontWeight: 'bold',
        margin: 8,
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
    },
});