import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet
} from 'react-native';
import MapView from 'react-native-maps';
import _ from 'underscore';
import randomColor from 'randomcolor';
import CustomCallout from './MapView/CustomCallout';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class NoteLocationScreen extends Component {

    _mapLocations() {
        return _.values(this.props.notes).map((note) => {
            console.log("::: MAP : " + JSON.stringify(note));
            return {
                latitude: note.location.coords.latitude,
                longitude: note.location.coords.longitude,
                title: note.title,
                description: note.body,
                key: note.id
            }
        });
    }

    render () {
        var locations = this._mapLocations();

        return (
            <MapView
                showsUserLocation={true}
                mapType={'hybrid'}
                initialRegion={{
                    latitude: _(locations).first().latitude,
                    longitude: _(locations).first().longitude,
                    latitudeDelta: 1.0922,
                    longitudeDelta: 1.0421,
                }}
                style={styles.map}>

                {locations.map((location) => {
                    //var backColor = randomColor();
                    var backColor = '#4da2ab';

                    return (
                        <MapView.Marker 
                            pinColor={randomColor()}
                            key={location.key}
//                            title={location.title}
//                            description={location.description}
                            coordinate={{latitude: location.latitude, longitude: location.longitude}}
                            onCalloutPress={() => this.props.navigator.push({
                                name: "createNote", 
                                note: {
                                    title: location.title, 
                                    body: location.description, 
                                    id: location.key
                                }
                            })}
                            calloutOffset={{ x: -8, y: 28 }}
                            calloutAnchor={{ x: 0.5, y: 0.4 }}>

                            <MapView.Callout tooltip style={styles.customView}>
                                <CustomCallout backgroundColor={backColor}>
                                    <View style={{flex:1, flexDirection: 'row'}}>
                                        <Icon style={{marginRight: 4, paddingLeft: -2}} name="information-outline" size={30} color="#EEFFD3" />
                                        <View style={{flex:1}}>
                                            <Text numberOfLines={1} style={{fontWeight: 'bold', fontSize: 16}}>{location.title}</Text>
                                            <Text numberOfLines={3}>{location.description}</Text>
                                        </View>
                                    </View>                                    
                                </CustomCallout>
                            </MapView.Callout>
                        </MapView.Marker>
                    );
                })}
            </MapView>
        );
    }
}

var styles = StyleSheet.create({
    map: {
        flex: 1,
        marginTop: 55,
        // position: 'absolute',
        // top: 0,
        // left: 0,
        // right: 0,
        // bottom: 0,
    },
    customView: {
        width: 220,
        minHeight: 100,
    },
});