import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  Navigator,
  StyleSheet,
  Text,
  View
} from 'react-native';
import _ from 'underscore';
import SimpleButton from './App/Components/SimpleButton';
import NoteScreen from './App/Components/NoteScreen';
import HomeScreen from './App/Components/HomeScreen';
import NoteLocationScreen from './App/Components/NoteLocationScreen';
import CameraScreen from './App/Components/CameraScreen';

export default class ReactNotes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialPosition: undefined,
      selectedNote: {title: "", body: ""},
      noteList: {
        1: {title: "Note 1", body: "Body 1", id: 1},
        2: {title: "Note 2", body: "Body 2", id: 2},
    }};
    this.loadNotes();
    this.trackLocation();

    console.log("state.noteList");
    console.log(this.state);
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }


  async saveNotes(notes) {
    try {
      AsyncStorage.setItem("@ReactNotes:notes", JSON.stringify(notes));
      console.log('::: SAVE @ReactNotes:notes' + JSON.stringify(notes));
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  async loadNotes() {
    try {
      var notes = await AsyncStorage.getItem("@ReactNotes:notes");
      if (notes !== null) {
        this.setState({noteList: JSON.parse(notes)});
        console.log('::: LOAD @ReactNotes:notes' + notes);
      }
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  updateNote(note) {
    if (!note.isSaved)
      note.location = this.state.initialPosition;

    note.isSaved = true;

    var newNotes = Object.assign({}, this.state.noteList);
    newNotes[note.id] = note;

    console.log('::: UPDATE: ' + JSON.stringify(newNotes));

    this.setState({noteList: newNotes});
    this.saveNotes(newNotes);
  }

  deleteNote(note) {
    var newNotes = Object.assign({}, this.state.noteList);
    delete newNotes[note.id];

    console.log('::: DELETE: ' + JSON.stringify(newNotes));

    this.setState({noteList: newNotes});
    this.saveNotes(newNotes);        
  }

  trackLocation() {
    navigator.geolocation.getCurrentPosition(
      (initialPosition) => this.setState({initialPosition}),
      (error) => alert(error.message)
    );

    this.watchID = navigator.geolocation.watchPosition((lastPosition) => this.setState({lastPosition}));
  }

  saveNoteImage(imagePath, note) {
    console.log("::: IMAGE " + imagePath);
    note.imagePath = imagePath;
    this.updateNote(note);
  }


  renderScene(route, navigator) {
    switch(route.name) {
      case 'home':
        console.log("home");
        console.log(this.state);

        return (
          <HomeScreen 
            navigator={navigator}
            notes={_(this.state.noteList).toArray()}
            onSelectNote={(note) => navigator.push({name: "createNote", note: note})}
            />
        );
      case 'createNote':
        return(
          <NoteScreen 
            navigator={navigator}
            note={route.note}
            onChangeNote={(note) => this.updateNote(note)} 
            showCameraButton={true}
            />
        );
      case 'noteLocations':
        return (
          <NoteLocationScreen
            navigator={navigator}
            notes={this.state.noteList}
            onSelectNote={(note) => navigator.push({name: "createNote", note: note})}
            />
        );
      case 'camera':
        return (
          <CameraScreen onPicture={(imagePath) => this.saveNoteImage(imagePath, route.note) } />
        );
    }
  }

  render() {
    return (
      <Navigator
        initialRoute={{name: 'home'}}
        //renderScene={this.renderScene}
        renderScene={this.renderScene.bind(this)}     // Goddamn lifecycle again : http://stackoverflow.com/questions/37524233/get-state-from-renderscene-function-navigator-in-react-native
        navigationBar={
          <Navigator.NavigationBar
            style={styles.navBar} 
            routeMapper={NavigationBarRouteMapper} />
        }
        onDeleteNote={(note) => this.deleteNote(note) }
        />
    );
  }
}

var NavigationBarRouteMapper = {
  LeftButton: function(route, navigator, index, navState) {
    switch (route.name) {
       case 'home':
        return (
          <SimpleButton
            onPress={() => navigator.push({name: 'noteLocations'})}
            customText='Map'
            style={styles.navBarLeftButton}
            textStyle={styles.navBarButtonText}
           />
        );
      case 'createNote':
      case 'noteLocations':
      case 'camera':
        return (
          <SimpleButton
            onPress={() => navigator.pop()}
            customText='Back'
            style={styles.navBarLeftButton}
            textStyle={styles.navBarButtonText}
           />
        );
      default:
        return null;
    }
  },

  RightButton: function(route, navigator, index, navState) {
    switch(route.name) {
      case 'home':
        return(
          <SimpleButton 
            onPress={() => {
              navigator.push({
                name: 'createNote', 
                note: {
                  title: "", 
                  body: "", 
                  id: new Date().getTime(),
                  isSaved: false
                }
              });
            }}
            customText='Create Note'
            style={styles.navBarRightButton}
            textStyle={styles.navBarButtonText}
          />
        );
      case 'createNote':
        if (route.note.isSaved) {
          return(
            <SimpleButton
              onPress = {() => {
                  navigator.props.onDeleteNote(route.note);
                  navigator.pop();
                }}
              customText='Delete'
              style={styles.navBarRightButton}
              textStyle={styles.navBarButtonText}
            />
          );
        } else {
          return null;
        }
      default:
        return null;
    }
  },

  Title: function(route, navigator, index, navState) {
    switch (route.name) {
      case 'home':
        return (
          <Text style={styles.navBarTitleText}>React Notes</Text>
        );
      case 'createNote':
        return (
          <Text style={styles.navBarTitleText}>{route.note ? route.note.title : 'Create note'}</Text>
        );
      case 'noteLocations':
        return (
          <Text style={styles.navBarTitleText}>Note Locations</Text>
        );
      case 'camera':
        return (
          <Text style={styles.navBarTitleText}>Take Picture</Text>
        );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  navContainer: {
    flex: 1
  },
  navBar: {
    backgroundColor: '#5B29C1',
    borderBottomColor: '#48209A',
    borderBottomWidth: 1,
    elevation: 8
  },
  navBarTitleText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '500',
    marginVertical: 16
  },
  navBarLeftButton: {
    paddingLeft: 10
  },
  navBarRightButton: {
    paddingRight: 10
  },
  navBarButtonText: {
    color: '#EEE',
    fontSize: 16,
    marginVertical: 16
  }
});

AppRegistry.registerComponent('ReactNotes', () => ReactNotes);
